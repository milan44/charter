const request = require('request');
const cheerio = require('cheerio');
const fs = require('fs');
const download = require('download-file');
const _cliProgress = require('cli-progress');
const rimraf = require("rimraf");

const AIRPORT_LIST = 'https://skyvector.com/airports/United%20States/';
let AIRPORT_ARRAY = [];
let QUEUE = [];

request(AIRPORT_LIST, async function (error, response, body) {
    const $ = cheerio.load(body);
    let regions = [];

    $('span.views-summary.views-summary-unformatted a').each(function() {
        regions.push('https://skyvector.com' + $(this).attr('href'));
    });

    const dataDir = './data/';

    if (!fs.existsSync(dataDir)) {
        fs.mkdirSync(dataDir);
    }

    if (!fs.existsSync('airports.json')) {
        console.log("Crawling regions...");
        const regionProgress = new _cliProgress.SingleBar({}, _cliProgress.Presets.shades_classic);
        regionProgress.start(regions.length, 0);

        for (let x = 0; x < regions.length; x++) {
            const region = regions[x];
            await loadAirportsFromRegion(region);

            regionProgress.update(x + 1);
        }
        regionProgress.stop();

        fs.writeFileSync('airports.json', JSON.stringify(AIRPORT_ARRAY));
    } else {
        AIRPORT_ARRAY = JSON.parse(fs.readFileSync('airports.json').toString());
        if (fs.existsSync('queue.json')) {
            AIRPORT_ARRAY = JSON.parse(fs.readFileSync('queue.json').toString());
        }

        QUEUE = AIRPORT_ARRAY;
    }

    console.log("Crawling airports...");
    const airportProgress = new _cliProgress.SingleBar({}, _cliProgress.Presets.shades_classic);
    airportProgress.start(AIRPORT_ARRAY.length, 0);

    for (let x=0;x<AIRPORT_ARRAY.length;x++) {
        const airport = AIRPORT_ARRAY[x];
        await loadAirportInformation(airport, dataDir);

        airportProgress.update(x+1);
        removeFromQueue(airport);
    }
    airportProgress.stop();
    console.log('Completed!');
});

function removeFromQueue(element) {
    let newQueue = [];
    QUEUE.forEach(function(el) {
        if (el !== element) {
            newQueue.push(el);
        }
    });
    QUEUE = newQueue;

    fs.writeFileSync('queue.json', JSON.stringify(QUEUE));
}

function fileEscape(name) {
    name = name.split('/').join('-');

    return name;
}

function loadAirportInformation(airportURL, dataDirectory) {
    return new Promise(function(resolve) {
        request(airportURL, async function (error, response, body) {
            const $ = cheerio.load(body);

            let icao = '';
            let name = $('#titlebgright').text().trim();

            let diagram = '';
            let supplement = '';

            $('.aptdata .aptdatatitle').each(function() {
                const text = $(this).text().trim();
                if (text.startsWith('Location Information for')) {
                    icao = text.replace('Location Information for ', '');
                } else if (text.endsWith('Airport Diagram')) {
                    const link = $('a', $(this).parent()).attr('href');
                    if (link && diagram === '') {
                        diagram = 'https://skyvector.com' + link;
                    }
                } else if (text === 'Chart Supplement') {
                    supplement = 'https://skyvector.com' + $('#aptafd a', $(this).parent()).attr('href');
                }
            });

            if (icao === '') {
                return resolve();
            }

            const directory = dataDirectory + icao + '/';
            if (fs.existsSync(directory)) {
                await rimraf.sync(directory);
            }

            fs.mkdirSync(directory);

            let charts = [];

            if (diagram !== '' || supplement !== '') {
                charts.push({
                    source: diagram !== '' ? diagram : supplement,
                    name: 'Airport Diagram or Chart Supplement',
                    dist: 'Airport Diagram or Chart Supplement.pdf',
                    parent: ''
                });
            }

            let infoCharts = [];
            $('.aptdata .apttpp a').each(function() {
                const pdf = $(this).attr('href');
                const name = $(this).text().trim();

                const parent = $(this).closest('.aptdata');
                const head = $('.aptdatatitle', parent).text().trim();

                charts.push({
                    source: pdf.startsWith('/') ? 'https://skyvector.com' + pdf : pdf,
                    name: name,
                    dist: fileEscape(name) + '.pdf',
                    parent: fileEscape(head)
                });

                infoCharts.push({
                    name: name,
                    location: directory + fileEscape(head) + '/' + fileEscape(name) + '.pdf'
                });
            });

            const info = {
                icao: icao,
                name: name,
                charts: infoCharts
            };
            fs.writeFileSync(directory + 'info.json', JSON.stringify(info));

            for(let x=0;x<charts.length;x++) {
                try {
                    await downloadChart(charts[x], directory);
                } catch(e) {
                    console.log('Failed to load airport "' + icao + '"!');
                    console.error(e);
                    console.log(charts[x]);
                }
            }

            resolve();
        });
    });
}

function downloadChart(chart, directory) {
    return new Promise(function(resolve, reject) {

        if (chart.parent !== '') {
            directory = directory + chart.parent + '/';
            if (!fs.existsSync(directory)) {
                fs.mkdirSync(directory);
            }
        }

        download(chart.source, {
            directory: directory,
            filename: chart.dist
        }, function(err) {
            if (err) {
                reject(err);
            } else {
                resolve();
            }
        });
    });
}

function loadAirportsFromRegion(regionURL) {
    return new Promise(function(resolve) {
        request(regionURL, function (error, response, body) {
            const $ = cheerio.load(body);

            $('.airportSearch .view-content td.views-field.views-field-title a').each(function() {
                const link = $(this).attr('href');
                AIRPORT_ARRAY.push('https://skyvector.com' + link);
            });

            resolve();
        });
    });
}